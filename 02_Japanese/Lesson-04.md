
Lesson 4
===

## 1.1 日本語

1. 彼女にあの話をしてあげてください。
1. 彼女にあのやさしい男の人の話をしてあげてください。
1. 彼女に、いつも遊ぶやさしい男の人の話をしてあげてください。
1. 彼女に、いつも遊ぶやさしい男の人が犬を見つけてくれた話をしてあげてください。
1. 時間があれば、...

## 1.2 English

1. Tell her that.
1. Tell her about the kind guy.
1. Tell her about the kind guy with who you always play.
1. Tell her that the kind guy with who you always play found your dog.
1. If you have time, ...

## 2.1 エクスプレッション 日本語

`then`: たら and と

- スーパーに寄ったらルームメイトがいました。
- スーパーに寄ったら街で作られたまずいチーズが売っていました。
- スーパーに寄ると友達と話しているきみがいました。

`if`: れば

- スーパーに寄ればバイトをしているルームメイトに会えます。
- スーパーに寄れば街で作られたまずいチーズが買えます。

## 2.2 Expressions English

`then`

- I dropped by the supermarket, then I saw the roommate.
- I dropped by the supermarket, then I saw bad cheese produced in the city was on sale.
- I dropped by the supermarket, then I saw you talking to your friend.

`if`

- If you drop by the supermarket, you can see the roommate working there.
- If you drop by the supermarket, you can buy bad cheese produced in the city.
