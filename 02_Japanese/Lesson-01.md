
Lesson 1
===

## 1.1 日本語

1. <font color="green">あの人は誰ですか?</font>
2. <font color="orange">あそこにいる</font><font color="green">人は誰ですか?</font>
3. <font color="orange">あそこにいる、</font><font color="purple">何か見ている</font><font color="green">人は誰ですか?</font>
4. <font color="orange">あそこにいる、</font><font color="purple">何か見ている</font><font color="green">人のこと、</font><font color="brown">知っていますか?</font>
5. <font color="orange">あそこにいる、</font><font color="purple">何か見ている</font><font color="green">人のこと</font><font color="brown">をご存知ですか?</font>
6. <font color="orange">あそこにいらっしゃる、</font><font color="purple">何か見ておられる</font><font color="green">方のこと</font><font color="brown">をご存知ですか?</font>

## 1.2 English

1. <font color="green">Who's that guy?</font>
2. <font color="green">Who's that guy</font> <font color="orange">standing there?</font>
3. <font color="green">Who's that guy</font> <font color="orange">standing there</font> and <font color="purple">looking at something?</font>
4. <font color="brown">Do you know</font> <font color="green">that guy</font> <font color="orange">standing there</font> and <font color="purple">looking at something?</font>
5. (With keigo for listener.)
6. (With keigo for the guy as well.)

## 2.1 エクスプレッション 日本語

- <font color="blue">ところで、</font>あの話はどうなってる? (前の話に全然関係ない場合)
- <font color="blue">ちなみに、</font>あの話はどうなってる? (前の話とすこし関係がある場合)
- 時間がある<font color="red">限り</font>これを読みたいです。

## 2.2 Expressions English

- <font color="blue">By the way,</font> how's that going? (Two patterns)
- I wanna read this <font color="red">as long as</font> I have time.

## 3.1 例題

- 店の前にいる人のこと、知っていますか?
- 木の下で寝ている、あの人のこと、知っていますか?
- あそこにいる、小さくて、服がださくて、頭が悪そうで、だけど髪がとてもきれいな女の子のこと、知っていますか?
- ちなみに、その隣りにいる男の子のことは知っていますか?
- ところで、おなかすいてない?

## 3.2 Practice

- Do you know that guy standing in front of the shop?
- Do you know that guy sleeping under the tree?
- Do you know that girl standing there, being short, with ugly fashion, seeming to be stupid, but with very beautiful hair?
- Btw, do you know that boy standing next to her?
- Btw, aren't you hungry?
