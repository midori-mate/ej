
Lesson 10
===


## 1.1 日本語

- あの犬は待っています。
- あの犬は男を待っています。
- あの白い犬は男を待っています。
- 海岸にいるあの白い犬は男を待っています。
- 海岸にいるあの白い犬は、男をずっと待っています。
- 海岸にいるあの白い犬は、もういない男をずっと待っている。


## 1.2 English

- The dog is waiting.
- The dog is waiting for the man.
- The white dog is waiting for the man.
- The white dog at the coast is waiting for the man.
- The white dog at the coast has been waiting for the man.
- The white dog at the coast has been waiting for the man who had gone.


## 2.1 例題

上の白い犬について何か考えて、3つ文章をつくってください。ただしそれぞれの文章は、以下の言葉から始めてください。

1. まず、 ...
1. さらに、 ...
1. 思うに、 ...


## 2.2 Practice

Create 3 sentences after thinking about the white dog described above. You have to start each sentence from these words below.

1. At first ...
1. Moreover ...
1. I guess ...
