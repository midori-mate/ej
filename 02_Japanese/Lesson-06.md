
Lesson 6
===


## 1.1 日本語

1. 言いにくいことがふたつあります。
1. 言いにくいことがふたつありますが、私は言わなければいけません。
1. 言いにくいことがふたつありますが、あなたが好きだから、私は言わなければいけません。
1. 料理について言いにくいことがふたつありますが、あなたが好きだから、私は言わなければいけません。
1. あなたの料理は今までとてもまずかったですが、今日のは特にまずいです。


## 1.2 English

1. I have two things that is hard to say.
1. I have two things that is hard to say but I have to say.
1. I have two things that is hard to say but I have to say cuz I love you.
1. I have two things about your meal that is hard to say but I have to say cuz I love you.
1. Your meal sucked so far, but today it notably does.


## 2.1 例題

- 留学についての情報が、さらにふたつあります。
- 外に出ていく人を、さらに三人見ました。
- 聞かなければいけないラジオを、さらにひとつ見つけました。
- 友達が、私の好きな本を、さらに5冊も渡してきました!
- 教授が、読みにくい本を、さらに5冊も渡してきました……。


## 2.2 Practice

- I have two more information about studying abroad.
- I saw three more men going out.
- I found one more radio channel to listen to!
- My friend gave me five more books what I like!
- My professor gave me five more books what are hard to read...
