
Lesson 2
===

## 1.1 日本語

1. 本を読むのは時間がかかるものだ。
2. ふつう、本を読むのは時間がかかるものだから、ゆっくりやりなさい。
3. ふつう、役に立つ本を読むのは時間がかかるものだから、ゆっくりやりなさい。
4. ふつう、人生の役に立つ本を読むのは時間がかかるものだから、ゆっくりやりなさい。
5. この本は時間をかけて読まれるべきものだ。
6. ふつう、人生の役に立つ本は時間をかけて読まれるべきものだから、ゆっくりやりなさい。

## 1.2 English

1. To read a book takes time.
2. Usually to read a book takes time, so do it easily.
3. Usually to read a book which's useful takes time, so do it easily.
4. Usually to read a book which's useful for your life takes time, so do it easily.
5. The book is to be read for a long time.
6. Usually the book which's useful for your life is to be read for a long time, so do so.

## 2.1 エクスプレッション 日本語

- あんまり無理するのはやめときなさい。
- あんまり授業をさぼるのはやめときなさい。

## 2.2 Expressions English

- Don't force yourself so much.
- Don't skip classes so often.

## 3.1 例題

- 彼の家に行くのは時間がかからないから、そんなに急ぐのはやめときなさい。
- あそこにいる、本を読んでる人をあんまり見るのはやめときなさい。

## 3.2 Practice

- To go to his house doesn't take time, so don't hurry so much.
- Don't look at that guy who is standing there and reading a book so much.
