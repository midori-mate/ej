
Lesson 7
===


## 1.1 日本語

1. 私はお酒を飲みません。
1. 平日は、私はお酒を飲みません。
1. 平日は、私はお酒を飲まないで、野菜を食べます。
1. 平日は、私はお酒を飲まないで、野菜とかナッツを食べます。
1. 平日は、私はお酒を飲まないで、野菜とかナッツを食べます。あと、運動もします。


## 1.2 English

1. I don't drink.
1. On weekdays, I don't drink.
1. On weekdays, I don't drink, I eat vegetable.
1. On weekdays, I don't drink, I eat vegetable and nuts.
1. On weekdays, I don't drink, I eat vegetable and nuts. Besides, I exercise as well.


## 2.1 例題

- 週末は、私は山に行って、絵をかきます。
- 友達といるときは、公園に行ったり、街に行ったりします。
- 来年は、アメリカとかカナダに行きたいです。
- 去年(きょねん)は、ミドリはデスクとか椅子を買いたいと言っていました。


## 2.2 Practice

- On the weekend, I go to mountains and paint.
- When I'm with friends, we go to the park or we go to the town.
- Next year, I'm willing to go to America and Canada.
- Last year, Midori said he wanted to buy a desk and chair.
