
Lesson 5
===


## 1.1 日本語

1. (日曜日) 今週、私は絵をかきます。
1. (月曜日) 今週、私は絵をかいています。
1. (火曜日) 今週、私はアキコの誕生日のための絵をかいています。
1. (水曜日) 今週、私はトオルの誕生日のための絵をかいていました。
1. (木曜日) 今週、私はユウの誕生日のための絵をかいていたと思う。
1. (金曜日) 今週、私はカツミの誕生日のための絵をかいていたはずだ。
1. (土曜日) 今週、私は誰かの誕生日のための絵をかいていたはずですよね……?


## 1.2 English

1. (Sunday)    This week, I'm willing to paint.
1. (Monday)    This week, I'm painting.
1. (Tuesday)   This week, I'm painting for Akiko's birthday.
1. (Wednesday) This week, I've been painting for Tooru's birthday.
1. (Thursday)  This week, I guess I was painting for Yuu's birthday.
1. (Friday)    This week, I'm sure I was painting for Katsumi's birthday.
1. (Saturday)  This week, I was painting for someone's birthday, wasn't I?


## 2.1 例題

- 駅へ行くあいだに、日が昇った。
- 走ったので、電車には間に合った。
- 電車の中で、ずっと前に私をぶった男の人に会った。
- そのことについては私が悪かったんだけど、彼はあらためて謝ってくれた。


## 2.2 Practice

- The sun rose while I was going to the station.
- I could take a train as I run.
- In the train I met the guy who hit me long before.
- Although it was my bad about that, he apologized me again.
