
Lesson 9
===


## 1.1 日本語

- 私は話し合いたかったです。
- 私は、明日のことを話し合いたかったです。
- 私は、ヤマザキさんと、明日のことを話し合いたかったです。
- 私は、ヤマザキさんと、明日のことを話し合いたかったです。しかし彼は見つかりませんでした。


## 1.2 English

- I wanted to discuss.
- I wanted to discuss for tmr.
- I wanted to discuss for tmr with Yamazaki-san.
- I wanted to discuss for tmr with Yamazaki-san, but I couldn't find him.


## 2.1 例題

きみはホンダさんとイトウさんを尊敬していて、彼らが来たら嬉しいと思っています。

1. 明日、ホンダさんは来てくれますか?
1. 明日は来ますか?
1. 昨日、イトウさんは来てくれましたか?
1. 昨日、ササキさんは来ましたか?


## 2.2 Practice

You respect Honda-san and Itou-san and will be glad if they come.

1. Does Honda-san come tmr?
1. Do you come tmr?
1. Did Itou-san come ystd?
1. Did Sasaki-san come ystd?
