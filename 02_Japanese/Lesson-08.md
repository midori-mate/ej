
Lesson 8
===


## 1.1 日本語

1. あのホテルは私を満足させました。
1. 新宿にあるあのホテルは私を満足させました。
1. 新宿にあるあのホテルは、とてもキレイで、私を満足させました。


## 1.2 English

1. That hotel satisfied me.
1. That hotel at Shinjuku satisfied me.
1. That hotel at Shinjuku, which was very clean, satisfied me.


## 2.1 例題

1. 私はどんなジュースも買いません。
1. 私は、私を満足させたジュースしか買いません。
1. あなたはどんな意見も受け入れません。
1. あなたは、自分を納得させた意見しか受け入れません。
1. あの子はどんな人とも結婚しません。
1. あの子は、彼女を負かした人としか結婚しません。


## 2.2 Practice

1. I won't buy any juice.
1. I won't buy any juice but that satisfy me.
1. You won't accept any opinions.
1. You won't accept any opinions but that persuade you.
1. That girl won't marry anyone.
1. That girl won't marry anyone but who beats her.
