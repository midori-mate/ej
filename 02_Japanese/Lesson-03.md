
Lesson 3
===

## 1.1 日本語

1. 時間だ!
1. 走る時間だ!
1. ようやく、ジムで走る時間だ!
1. ようやく、いつも行くジムで走る時間だ!
1. ようやく、いつも行くジムで走る時間だよね?

## 1.2 English

1. It's the time.
1. It's the time to run.
1. Finally it's the time to run at the gym.
1. Finally it's the time to run at the gym where we usually go.
1. Finally it's the time to run at the gym where we usually go, isn't it?

## 2.1 エクスプレッション 日本語

- 彼は子供っぽいよね。
- 彼は子供だし、子供らしいよね。たとえば素直に笑う。
- 彼は大人なのに、子供みたいだよね。たとえば外で騒ぐ。

## 2.2 Expressions English

- He is childish, isn't he?
- He is a child, and just like so. He laughs honestly for example.
- He is an adult, but is childish. He makes noice outside for example.

## 3.1 例題

- 今日は雨っぽいよね、すごくくもってる。
- 今日は雨らしいよね。いつも見るラジオで言ってた。
- 今日は雨みたいだよね。学校で会った友達が言ってた。

## 3.2 Practice

- Today, it seems to rain. It's so cloudy.
- Today, it seems to rain. The news I always watch said.
- Today, it seems to rain. My friend told me at the school.
